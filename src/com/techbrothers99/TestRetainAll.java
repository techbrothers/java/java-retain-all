package com.techbrothers99;

import java.util.ArrayList;
import java.util.List;

public class TestRetainAll {
	
	public static void main(String[] args) {
		
		
		List<String> list1 = new ArrayList<String>();
		list1.add("pen");
		list1.add("pencil");
		list1.add("paper");
		list1.add("book");
		list1.add("scale");
		
		List<String> list2 = new ArrayList<String>();
		list2.add("pen");
		list2.add("pencil");
		list2.add("paper");
		list2.add("Eraser");
		
		System.out.println("List1 elements" + list1);
		System.out.println("List2 elements" + list2);
		
		list1.retainAll(list2);
		
		System.out.println("List1 elements" + list1);
		System.out.println("List2 elements" + list2);
		
		list2.retainAll(list1);
		
		System.out.println("List1 elements" + list1);
		System.out.println("List2 elements" + list2);
		
	}

}
